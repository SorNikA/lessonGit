//
//  ViewController.swift
//  lessonGit
//
//  Created by Никита Сорокин on 15.05.2022.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // code 1
        /*
         
         code
         code
         code...
         
         */
        
        // code 2 new
        /*
         
         code 2 new
         code 2 new
         code 2 new...
         
         */
        
        // app store 1

        // fix bugs
        
        // code 3 new
        /*
         
         code 3 new
         code 3 new
         code 3 new...
         
         */
        
        // app store 2
        sayHello()
    }
    private func sayHello(){
        print("Hello")
    }


}

